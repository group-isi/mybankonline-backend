package com.isj.isi.Mybankbackend;

import com.isj.isi.Mybankbackend.dto.DepotDto;
import com.isj.isi.Mybankbackend.dto.ManagerDto;
import com.isj.isi.Mybankbackend.entity.Depot;
import com.isj.isi.Mybankbackend.entity.Manager;
import com.isj.isi.Mybankbackend.repositories.ManagerRepository;
import com.isj.isi.Mybankbackend.services.DepotService;
import com.isj.isi.Mybankbackend.utils.GenerateCodeUtils;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@SpringBootTest
class MybankBackendApplicationTests {

	PropertyMap<ManagerDto, Manager> skipidcode= new PropertyMap<ManagerDto, Manager>() {
		@Override
		protected void configure() {
			skip().setCode(null);

		}
	};


	@Autowired
	private DepotService service;

	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	JavaMailSender emailSender;

	@Autowired
	private ManagerRepository m;
	public void envoyerMailVerification(String mail, String contenu) throws MessagingException {
		MimeMessage message= emailSender.createMimeMessage();

		boolean multipart= true;

		MimeMessageHelper helper= new MimeMessageHelper(message,multipart,"utf-8");

		String htmlMsg = contenu;

		message.setContent(htmlMsg, "text/html");

		helper.setTo(mail);

		helper.setSubject("Activation du compte Bank");


		this.emailSender.send(message);

	}

	@SneakyThrows
	@Test
	void contextLoads() throws MessagingException {
		//ManagerDto managerDto = new ManagerDto("mich", "laura", "etudiant", "marriée");
		//Manager manager = modelMapper.map(modelMapper, Manager.class);
		System.out.println(m.findByEmail("wilfriedbobi@gmail.com"));
		System.out.println(GenerateCodeUtils.generateCode());

		//envoyerMailVerification("payeplionel2@outlook.com","message");

		DepotDto depot1 = new DepotDto();
		depot1.setAccountNumber("vPUzHUZt2Ks");
		depot1.setMontant(1000L);
		Depot depot = service.addDepot(depot1);
		System.out.println(depot.toString());
	}

}
