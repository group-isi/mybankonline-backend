package com.isj.isi.Mybankbackend.mappers;


import com.isj.isi.Mybankbackend.dto.ManagerDto;
import com.isj.isi.Mybankbackend.entity.Manager;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface ManagerMapper {


    @Mapping(target = "id", ignore = true)
    @Mapping(target = "code", ignore = true)

    public Manager toEntity(ManagerDto managerDto);

}
