package com.isj.isi.Mybankbackend.mappers;


import com.isj.isi.Mybankbackend.dto.ClientDto;
import com.isj.isi.Mybankbackend.entity.Client;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;


@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")

public class ClientMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "code", ignore = true)

    public Client toEntity(ClientDto clientDto) {
        return null;
    }
}
