package com.isj.isi.Mybankbackend.dto;


import com.isj.isi.Mybankbackend.entity.Abstract_Mybank_Entity;
import lombok.*;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class VirementDto {

    private  String senderaccountNumber;
    private String receiveraccountNumber;

    private Float montant;

    private String motifVirement;
}
