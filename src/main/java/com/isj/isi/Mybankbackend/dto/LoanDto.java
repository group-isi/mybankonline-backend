package com.isj.isi.Mybankbackend.dto;


import lombok.*;


@Data
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class LoanDto {
    private String name;
    private String surname;
    private String sexe;
    private Float Salaire;
    private String accountNumber;
    private Float amountRequest;
    private String refundMethod;
}
