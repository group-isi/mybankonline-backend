package com.isj.isi.Mybankbackend.dto;


import lombok.*;

import javax.validation.constraints.Email;
import java.util.Date;



@Data
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class ManagerDto {

    private  String name;
    //@NotEmpty
    private String surname;
    //@NotEmpty
    private String birthday;
    //@NotEmpty
    private String profession;
    //@NotEmpty
    private String matrimonial_status;
    //  @NotEmpty
    private String address;
    //@NotEmpty
    @Email
    private String email;
    // @NotEmpty
    private String phone_number;
    //@NotEmpty
     private int cni;

    private String password;

    public ManagerDto(String name, String surname,String profession, String matrimonial_status) {
        this.name = name;
        this.surname = surname;
        this.profession = profession;
        this.matrimonial_status = matrimonial_status;
    }
}
