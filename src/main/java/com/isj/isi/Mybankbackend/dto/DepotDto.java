package com.isj.isi.Mybankbackend.dto;

import lombok.*;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class DepotDto {

    private String accountNumber;

    private Long montant;

    private String espece;


}
