package com.isj.isi.Mybankbackend.entity;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


@Data
@NoArgsConstructor
@EqualsAndHashCode
@MappedSuperclass
@Getter
@Setter
public class User extends Abstract_Mybank_Entity {

   // @NotEmpty
    private  String name;
   // @NotEmpty
    private String surname;
    //@NotEmpty
    //private String birthday;
    //@NotEmpty
    private String profession;
    //@NotEmpty
    private String matrimonial_status;
    //@NotEmpty
    private String address;
    //@NotEmpty
   // @Email
    private String email;
    //@NotEmpty
    private String phone_number;

    @OneToMany(cascade =CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",joinColumns = @JoinColumn(name = "idUser"),
            inverseJoinColumns = @JoinColumn(name = "idRole"))

    private List<Bank_Role> bank_roles = new ArrayList<>();
/*
 @Override
 public String toString() {
  return "User{" +
          "name='" + name + '\'' +
          ", surname='" + surname + '\'' +
          ", birthday=" + birthday +
          ", profession='" + profession + '\'' +
          ", matrimonial_status='" + matrimonial_status + '\'' +
          ", address='" + address + '\'' +
          ", email='" + email + '\'' +
          ", phone_number='" + phone_number + '\'' +
          ", bank_roles=" + bank_roles +
          ", id=" + id +
          ", code='" + code + '\'' +
          '}';
 }*/
}

