package com.isj.isi.Mybankbackend.entity;


import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
@EqualsAndHashCode
@NoArgsConstructor
@Getter
@Setter
public class Depot extends Abstract_Mybank_Entity {

    private String accountNumber;

    private float montant;

    private String espece;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name ="account")
    private Account account;



}
