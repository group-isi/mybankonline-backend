package com.isj.isi.Mybankbackend.entity;



import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
@EqualsAndHashCode
@NoArgsConstructor
@Getter
@Setter
public class Retrait extends Abstract_Mybank_Entity {

    private String name;
    private String surname;
    private String accountNumber;
    //private String secretCode;
    private Float montant;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name ="account")
    private Account account;
}
