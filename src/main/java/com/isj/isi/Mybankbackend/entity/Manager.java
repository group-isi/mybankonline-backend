package com.isj.isi.Mybankbackend.entity;



import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "bank_manager")
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Data
public class Manager extends User {
   // @NotEmpty
    private int cni;

    //@NotEmpty
    private String password;

    private String birthday;

    @Override
    public String toString() {
        return "Manager{" +
                "cni='" + cni + '\'' +
                ", password='" + password + '\'' +
                ", id=" + id +
                ", code='" + code + '\'' +
                '}';
    }
}
