package com.isj.isi.Mybankbackend.entity;

import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class Virement extends Abstract_Mybank_Entity {

    private  String senderaccountNumber;
    private String receiveraccountNumber;
     private Float montant;
     private String motifVirement;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "account")
     private Account account;

}

