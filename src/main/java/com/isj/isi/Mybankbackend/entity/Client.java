package com.isj.isi.Mybankbackend.entity;



import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "bank_client")
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Data
public class Client extends User {
   // @NotEmpty
    private String cni;
    //@NotEmpty
    private String photo;
    //@NotEmpty
    private String localisation;

      private String birthday;



}
