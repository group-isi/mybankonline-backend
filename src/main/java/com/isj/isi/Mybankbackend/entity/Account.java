package com.isj.isi.Mybankbackend.entity;

import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "bank_account")
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@ToString
public class Account  extends Abstract_Mybank_Entity {

    //@NotEmpty
    @Column(unique = true)
    private String accountNumber;
    // @NotEmpty
    private float balanceAccount;

    //@NotEmpty
    private String status;


    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "manager")
    private Manager manager;


    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "client")
    private Client client;



}


