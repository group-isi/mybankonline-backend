package com.isj.isi.Mybankbackend.entity;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Data
@NoArgsConstructor
@EqualsAndHashCode
@Entity
public class Demand_Suppression extends Abstract_Mybank_Entity {
    // @NotEmpty

    private String accountNumber;
    // @NotEmpty
    private String name;

    // @NotEmpty
    private String motif_suppression;

    private String status;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "manager")
    private Manager manager;


    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "client")
    private Client client;


}


