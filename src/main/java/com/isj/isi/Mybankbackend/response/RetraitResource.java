package com.isj.isi.Mybankbackend.response;

import com.isj.isi.Mybankbackend.dto.RetraitDto;
import com.isj.isi.Mybankbackend.services.RetraitService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


@RestController
@RequestMapping("api/retrait")
@Api("Api pour les opérations de virement")
public class RetraitResource extends AbstractEnpointResource {


    @Autowired
    private RetraitService retraitService;


    @PostMapping("/save")
    public ResponseEntity create (@RequestBody @Valid RetraitDto retraitDto, HttpServletResponse response){

        logger.info("POST/api/retrait/save called");

        try{
            response.setStatus((HttpStatus.CREATED.value()));
            return ResponseEntity.ok(retraitService.addRetrait(retraitDto));

        } catch (Exception e) {
            logger.error("l'erreur est : " + e.getMessage());
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            //return ResponseEntity.unprocessableEntity().build();
            return ResponseEntity.ok().build();
        }
    }




}
