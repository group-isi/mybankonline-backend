package com.isj.isi.Mybankbackend.response;

import com.isj.isi.Mybankbackend.dto.DemandDto;
import com.isj.isi.Mybankbackend.entity.Demand_Suppression;
import com.isj.isi.Mybankbackend.services.DemandService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("api/demands")
@Api("API pour les opérations de demande de suppression")

public class DemandResource extends AbstractEnpointResource {


    @Autowired
    private DemandService demandService;


    @PostMapping("/save")
    public ResponseEntity create (@RequestBody @Valid DemandDto demandDto,
                                  HttpServletResponse response){

        logger.info("POST /api/demands/save called");

        try {
            response.setStatus(HttpStatus.CREATED.value());
            return ResponseEntity.ok(demandService.addDemand(demandDto));
        } catch ( Exception e){
            logger.error(e.getMessage());
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            //return ResponseEntity.unprocessableEntity().build();
            return ResponseEntity.ok().build();
        }
    }



    //affiche tous les clients qui font la demande d'une création de compte sur le dashboard
    @GetMapping("/listDemands")
    public  ResponseEntity<List<Demand_Suppression>> getAllDemands(@RequestParam   (defaultValue="0")int page, @RequestParam(defaultValue = "100") int size){

        try{
            return ResponseEntity.ok(demandService.listDemandSuppression(PageRequest.of(page, size)).getContent());
        } catch (Exception e) {
            e.printStackTrace();
            return  ResponseEntity.unprocessableEntity().build();
        }

    }


}
