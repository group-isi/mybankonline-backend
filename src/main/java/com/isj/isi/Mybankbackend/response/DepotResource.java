package com.isj.isi.Mybankbackend.response;

import com.isj.isi.Mybankbackend.dto.DepotDto;
import com.isj.isi.Mybankbackend.services.DepotService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Access;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;



@RestController
@RequestMapping("api/depot")
@Api("API pour les opérations de dépot")

public class DepotResource extends AbstractEnpointResource {


    @Autowired
    private DepotService depotService;


    @PostMapping("/save")
    public ResponseEntity create (@RequestBody @Valid DepotDto depotDto, HttpServletResponse response){

        logger.info("POST/api/depot/save called");

        try{
            response.setStatus((HttpStatus.CREATED.value()));
            return ResponseEntity.ok(depotService.addDepot(depotDto));

        } catch (Exception e) {
            logger.error("l'erreur est : " + e.getMessage());
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            //return ResponseEntity.unprocessableEntity().build();
            return ResponseEntity.ok().build();
        }
    }

}
