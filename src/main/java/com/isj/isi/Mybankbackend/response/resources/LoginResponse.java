package com.isj.isi.Mybankbackend.response.resources;




import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Data

/**
 * Instantiates a new login response.
 */
@NoArgsConstructor
public class LoginResponse {


    /** The access token. */
    private String accessToken;


    /** The user. */
    private Object user;



}