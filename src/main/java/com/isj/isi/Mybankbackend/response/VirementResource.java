package com.isj.isi.Mybankbackend.response;


import com.isj.isi.Mybankbackend.dto.VirementDto;
import com.isj.isi.Mybankbackend.services.VirementService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("api/virement")
@Api("API pour les opérations de Virement")
public class VirementResource extends AbstractEnpointResource {


    @Autowired
    private VirementService virementService;



    @PostMapping("/save")
    public ResponseEntity create (@RequestBody @Valid VirementDto virementDto, HttpServletResponse response){

        logger.info("POST/api/virement/save called");

        try{
            response.setStatus((HttpStatus.CREATED.value()));
            return ResponseEntity.ok(virementService.addVirement(virementDto));

        } catch (Exception e) {
            logger.error("l'erreur est : " + e.getMessage());
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            //return ResponseEntity.unprocessableEntity().build();
            return ResponseEntity.ok().build();
        }
    }


}
