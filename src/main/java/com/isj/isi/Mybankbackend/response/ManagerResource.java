package com.isj.isi.Mybankbackend.response;

import com.isj.isi.Mybankbackend.dto.ManagerDto;
import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Demand_Suppression;
import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import com.isj.isi.Mybankbackend.services.ManagerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("api/managers")
@Api("API pour les opérations sur les managers")
public class ManagerResource extends AbstractEnpointResource {

    @Autowired
    private ManagerService managerService;


    @PostMapping("/save")
    public ResponseEntity create (@RequestBody @Valid ManagerDto managerDto, HttpServletResponse response){

        logger.info("POST/api/managers called");

        try{
            response.setStatus((HttpStatus.CREATED.value()));
            return ResponseEntity.ok(managerService.addManager(managerDto));

        } catch (Exception e) {
            logger.error("l'erreur est : " + e.getMessage());
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            //return ResponseEntity.unprocessableEntity().build();
            return ResponseEntity.ok().build();
        }
    }


    //affiche tous les clients qui font la demande d'une création de compte sur le dashboard
    @GetMapping("/listAccount")
    public  ResponseEntity<List<Account>> getAllAccounts(@RequestParam   (defaultValue="0")int page, @RequestParam(defaultValue = "100") int size){

        try{
            return ResponseEntity.ok(managerService.findAccountByManager(PageRequest.of(page, size)).getContent());
        } catch (Exception e) {
            e.printStackTrace();
            return  ResponseEntity.unprocessableEntity().build();
        }

    }

    //pour valider la demande de création d'un compte via le numéro de compte
@PutMapping("/validateAccount/{accountNumber}")
@ApiOperation(value = "valider un compte")
    public ResponseEntity<Account> validateAccount(@PathVariable String accountNumber, HttpServletResponse response){

        logger.info("PUT/api/validateAccount/accountNumber");

        try{
            response.setStatus(HttpStatus.OK.value());

            return ResponseEntity.ok(managerService.ValidateAccount(accountNumber));

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            return ResponseEntity.unprocessableEntity().build();
        }

}


    //pour valider la demande de suppression d'un compte via le numéro de compte
    @PutMapping("/deleteAccount/{accountNumber}")
    @ApiOperation(value = "supprimer un  compte")
    public ResponseEntity<Demand_Suppression> deleteAccount(@PathVariable String accountNumber, HttpServletResponse response){

        logger.info("PUT/api/deleteAccount/accountNumber");

        try{
            response.setStatus(HttpStatus.OK.value());
            return ResponseEntity.ok(managerService.validateDemandSuppression(accountNumber));

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            return ResponseEntity.unprocessableEntity().build();
        }

    }



}
