package com.isj.isi.Mybankbackend.response;

import com.isj.isi.Mybankbackend.dto.LoanDto;
import com.isj.isi.Mybankbackend.services.LoanService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


@RestController
@RequestMapping("/api/loan")
@Api("Api pour les opérations sur les prêt ")
public class LoanResource extends AbstractEnpointResource {

    @Autowired
    private LoanService loanService;


    @PostMapping("/save")
    public ResponseEntity create (@RequestBody @Valid LoanDto loanDto, HttpServletResponse response){

        logger.info("POST/api/virement/save called");

        try{
            response.setStatus((HttpStatus.CREATED.value()));
            return ResponseEntity.ok(loanService.addLoan(loanDto));

        } catch (Exception e) {
            logger.error("l'erreur est : " + e.toString());
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            //return ResponseEntity.unprocessableEntity().build();
            return ResponseEntity.ok().build();
        }
    }
}
