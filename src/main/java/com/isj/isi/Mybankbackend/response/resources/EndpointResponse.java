package com.isj.isi.Mybankbackend.response.resources;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EndpointResponse {

    /** The message. */
    private String message;

    /** The data. */
    private Object data;

    /** The status. */
    private String status;

}