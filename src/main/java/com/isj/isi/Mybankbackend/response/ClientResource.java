package com.isj.isi.Mybankbackend.response;

import com.isj.isi.Mybankbackend.dto.ClientDto;
import com.isj.isi.Mybankbackend.entity.Client;
import com.isj.isi.Mybankbackend.services.ClientService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;



@RestController
@RequestMapping("api/clients")
@Api("API pour les opérations sur les clients")
public class ClientResource extends AbstractEnpointResource {


    @Autowired
    private ClientService clientService;


    @PostMapping("/save")
    public ResponseEntity create (@RequestBody @Valid ClientDto clientDto,
                                          HttpServletResponse response){

        logger.info("POST /api/clients/save called");

        try {
            response.setStatus(HttpStatus.CREATED.value());
            return ResponseEntity.ok(clientService.addClient(clientDto));
        } catch ( Exception e){
            logger.error(e.getMessage());
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            //return ResponseEntity.unprocessableEntity().build();
            return ResponseEntity.ok().build();
        }
    }

}
