package com.isj.isi.Mybankbackend.response;



import com.isj.isi.Mybankbackend.response.resources.EndpointResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;





public abstract class AbstractEnpointResource {

    protected final Logger logger = LoggerFactory.getLogger(AbstractEnpointResource.class);

    protected EndpointResponse response;

    protected EndpointResponse success(Object data) {
        response = new EndpointResponse();
        response.setData(data);
        response.setMessage("Successful");
        response.setStatus(HttpStatus.OK.toString());
        return response;
    }

    protected EndpointResponse failure(Object data, HttpStatus status) {
        response = new EndpointResponse();
        response.setData(data);
        response.setMessage("Opération échouée");
        response.setStatus(status.toString());
        return response;
    }

    protected EndpointResponse failure(String message,Object data, HttpStatus status) {
        response = new EndpointResponse();
        response.setData(data);
        response.setMessage(message);
        response.setStatus(status.toString());
        return response;
    }
}


