package com.isj.isi.Mybankbackend.utils;




import java.security.SecureRandom;
import java.util.Base64;
import java.util.Base64.Encoder;

public class GenerateCodeUtils {

    /**
     * Instantiates a new generate code utils.
     */
    private GenerateCodeUtils() {
    }

    /**
     * Generate secret key.
     *
     * @return the string
     */
    public static String generateSecretKey() {
        final SecureRandom random = new SecureRandom();
        final byte[] bytes = new byte[20];
        random.nextBytes(bytes);
        final Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        return encoder.encodeToString(bytes);
    }

    public static String generateCode(int length) {
        final SecureRandom random = new SecureRandom();
        final byte[] bytes = new byte[length];
        random.nextBytes(bytes);
        final Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        return encoder.encodeToString(bytes);
    }

    public static String generateCode() {
        final SecureRandom random = new SecureRandom();
        final byte[] bytes = new byte[8];
        random.nextBytes(bytes);
        final Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        return encoder.encodeToString(bytes);
    }
}