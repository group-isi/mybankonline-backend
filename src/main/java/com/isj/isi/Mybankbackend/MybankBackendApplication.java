package com.isj.isi.Mybankbackend;

import com.isj.isi.Mybankbackend.dto.ManagerDto;
import com.isj.isi.Mybankbackend.entity.Manager;
import com.isj.isi.Mybankbackend.mappers.ManagerMapper;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MybankBackendApplication {

	@Bean
	public ManagerMapper managerMapper() {return new ManagerMapper() {
		@Override
		public Manager toEntity(ManagerDto managerDto) {
			return null;
		}
	};}

	@Bean
	public ModelMapper modelMapper() {return new ModelMapper();}

	public static void main(String[] args) {
		SpringApplication.run(MybankBackendApplication.class, args);
	}

}
