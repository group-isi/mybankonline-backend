package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.VirementDto;
import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Virement;
import com.isj.isi.Mybankbackend.repositories.AccountRepository;
import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import com.isj.isi.Mybankbackend.repositories.VirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@Service
public class VirementService implements VirementInterface {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private VirementRepository virementRepository;

    private Account account;

    @Autowired
    private JavaMailSender emailSender;


    public void envoyerMailVerification(String mail, String contenu) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

        String htmlMsg = contenu;

        message.setContent(htmlMsg, "text/html");

        helper.setTo(mail);

        helper.setSubject("Confirm the transfert");


        this.emailSender.send(message);

    }


    @Override
    public Virement addVirement(VirementDto virementDto) throws Exception {

        account = accountRepository.findByAccountNumber(virementDto.getReceiveraccountNumber());
        //celui qui envoie le virement
        Account account1= accountRepository.findByAccountNumber(virementDto.getSenderaccountNumber());

        System.out.println(account.toString());
        if (account == null) {
            throw new Exception("ce compte n'existe pas");

        }
        Virement virement = new Virement();

        String text = "Hello Mr/Mrs" + account.getClient().getName() + "," + "You have received a transfert of"+ virementDto.getMontant() +
                "please verify your balance account. ";

        String text1 = "Hello Mr/Mrs" + account1.getClient().getName() + "," + "You have done a transfert of"+ virementDto.getMontant() +
                "to" +account.getClient().getName()+ "please verify your balance account. ";

        try {
            virement.setReceiveraccountNumber(virementDto.getReceiveraccountNumber());
            virement.setMontant(virementDto.getMontant());
            virement.setMotifVirement(virementDto.getMotifVirement());
            virement.setSenderaccountNumber(virementDto.getSenderaccountNumber());
            float a = virementDto.getMontant();
            float b = account.getBalanceAccount();
            account.setBalanceAccount(a + b);
            accountRepository.save(account);
            virement = virementRepository.save(virement);

            envoyerMailVerification(account.getClient().getEmail(), text);
            // envoie de l'email à l'emmetteur
        envoyerMailVerification(account1.getClient().getEmail(), text);
            return virement;

        } catch (Exception ex) {
            ex.printStackTrace();
            //System.out.println(ex.toString());
            return null;
        }
    }

}


