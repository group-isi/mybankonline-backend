package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.ClientDto;
import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Bank_Role;
import com.isj.isi.Mybankbackend.entity.Client;
import com.isj.isi.Mybankbackend.repositories.AccountRepository;
import com.isj.isi.Mybankbackend.repositories.Bank_Role_Repository;
import com.isj.isi.Mybankbackend.repositories.ClientRepository;
import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import com.isj.isi.Mybankbackend.utils.GenerateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ClientService implements ClientInterface {


    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AccountRepository accountRepository;


    @Autowired
    private Bank_Role_Repository bank_role_repository;


    public Client addClient(ClientDto clientDto) throws Exception {

        Client client = new Client();
        Account account = new Account();


      /*  client= clientRepository.findByEmail(clientDto.getEmail());
if (client!=null){
    throw new Exception("cet email appartient déjà à un client");

}
*/

        try {
            client.setName(clientDto.getName());
            client.setSurname(clientDto.getSurname());
            client.setEmail(clientDto.getEmail());
            client.setMatrimonial_status(clientDto.getMatrimonial_status());
            client.setBirthday(clientDto.getBirthday());
            client.setProfession(clientDto.getProfession());
            client.setPhone_number(clientDto.getPhone_number());
            client.setCni(clientDto.getCni());
            client.setPhoto(clientDto.getPhoto());
            client.setAddress(clientDto.getAddress());
            client.setLocalisation(clientDto.getLocalisation());
            client.setCode(GenerateCodeUtils.generateCode());
            System.out.println("enregistrer client");
            String role = "client";
            Bank_Role bank_role = bank_role_repository.findByRole(role);
            List<Bank_Role> bank_roles = new ArrayList<>();
            client.setBank_roles(bank_roles);
            // Création du compte
            account.setCode(GenerateCodeUtils.generateCode());
            account.setAccountNumber(GenerateCodeUtils.generateCode());
            //long montant = 100000;
            //account.setBalanceAccount(montant);
            account.setClient(client);
            //Manager manager = new Manager();
            //account.setManager(manager);
            account.setStatus("inactif");

            client = clientRepository.save(client);

            accountRepository.save(account);




            return client;

        } catch (Exception ex) {
            ex.printStackTrace();

            return null;
        }


    }
}
