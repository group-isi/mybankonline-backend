package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.DemandDto;
import com.isj.isi.Mybankbackend.entity.Demand_Suppression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DemandInterface {

    public Demand_Suppression addDemand (DemandDto demandDto) throws Exception;

    public Page<Demand_Suppression> listDemandSuppression (Pageable peageRequest) throws Exception;
}
