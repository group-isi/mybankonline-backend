package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.ClientDto;
import com.isj.isi.Mybankbackend.entity.Client;

public interface ClientInterface {
    public Client addClient(ClientDto clientDto) throws Exception;
}
