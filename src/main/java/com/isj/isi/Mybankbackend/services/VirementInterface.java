package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.VirementDto;
import com.isj.isi.Mybankbackend.entity.Virement;

public interface VirementInterface {

    public Virement addVirement(VirementDto virementDto)throws Exception;

}
