package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.RetraitDto;
import com.isj.isi.Mybankbackend.entity.Retrait;

public interface RetraitInterface {

    public Retrait addRetrait (RetraitDto retraitDto) throws Exception;
}
