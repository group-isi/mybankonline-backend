package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.LoanDto;

import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Loan;
import com.isj.isi.Mybankbackend.repositories.AccountRepository;


import com.isj.isi.Mybankbackend.repositories.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;

@Service
public class LoanService implements LoanInterface {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private JavaMailSender emailSender;

    private Account account;

    @Autowired
    private LoanRepository loanRepository;


    public void envoyerMailVerification(String mail, String contenu) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

        String htmlMsg = contenu;

        message.setContent(htmlMsg, "text/html");

        helper.setTo(mail);

        helper.setSubject("Confirm the  Repository");


        this.emailSender.send(message);

    }



    @Override
    public Loan addLoan(LoanDto loanDto) throws Exception {

        Loan loan = new Loan();


        try {
            System.out.println("hello");
            account = accountRepository.findByAccountNumber(loanDto.getAccountNumber());
            System.out.println(account.toString());
            //Loan loan = new Loan();

            loan.setName(loanDto.getName());
            //System.out.println(loan.toString());
            loan.setSurname(loanDto.getSurname());
            loan.setSexe(loanDto.getSexe());
            //System.out.println(loan.toString());
            loan.setRefundMethod(loanDto.getRefundMethod());
            loan.setSalaire(loanDto.getSalaire());
            loan.setAccountNumber(loanDto.getAccountNumber());
            loan.setAmountRequest(loanDto.getAmountRequest());

            // calcul de l'age
            LocalDate date = LocalDate.parse(account.getClient().getBirthday());
            Period age = Period.between(date, LocalDate.now());
            int ageclient = age.getYears();
            System.out.println(ageclient);
            float salaireannuelle = (loanDto.getSalaire()) * 12;
            float salaireclient = loan.getSalaire();

            double limite = salaireannuelle * (70 - ageclient) * 0.5;
            System.out.println(loan.toString());
            System.out.println(ageclient);
            System.out.println(limite);


            if (18 < ageclient && ageclient < 65 && salaireclient > 150000 && loan.getAmountRequest() < limite) {
                loan = loanRepository.save(loan);
                String text = "Hello Mr/Mrs" + account.getClient().getName() + "," + "Your loan of "+ loan.getAmountRequest()+  "has been approuved ." +
                        "please verify your balance account ";
                envoyerMailVerification(account.getClient().getEmail(),text);
                return loan;
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }

        return null;
    }

}
