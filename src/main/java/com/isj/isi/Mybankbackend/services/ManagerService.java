package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.ManagerDto;
import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Bank_Role;
import com.isj.isi.Mybankbackend.entity.Demand_Suppression;
import com.isj.isi.Mybankbackend.entity.Manager;
import com.isj.isi.Mybankbackend.mails.EmailServiceImpl;
import com.isj.isi.Mybankbackend.repositories.*;
import com.isj.isi.Mybankbackend.utils.GenerateCodeUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;

import java.util.List;


@Service

public class ManagerService implements ManagerInterface {

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private Bank_Role_Repository bank_role_repository;

    @Autowired
    private AccountRepository accountRepository;


    private EmailServiceImpl emailService = new EmailServiceImpl();

    @Autowired
    private DemandRepository demandRepository;

    @Autowired
    private JavaMailSender emailSender;


    //@Autowired
    //private ManagerMapper managerMapper;
    //@Autowired
    //private ModelMapper modelMapper;

    // @Autowired
    // private ModelMapper modelMapper;
    /*PropertyMap<ManagerDto, Manager>  skipidcode= new PropertyMap<ManagerDto, Manager>() {
        @Override
        protected void configure() {
            skip().setCode(null);
        }
    };*/

    public Manager addManager(ManagerDto managerDto) throws Exception {


        Manager manager = new Manager();
        System.out.println("dans le manager service " + managerDto.toString());
        //System.out.println(manager.toString());
        /*if (managerRepository.findByEmail(managerDto.getEmail()) != null){
            throw new Exception("cet email appartient déjà à un client");
        }else
            manager = new Manager();*/

        System.out.println(manager);

        try {
            // manager = managerMapper.toEntity(managerDto);
            //manager= modelMapper.map(managerDto,Manager.class);
            System.out.println(manager);

            manager.setName(managerDto.getName());
            manager.setSurname(managerDto.getSurname());
            manager.setEmail(managerDto.getEmail());
            manager.setAddress(managerDto.getAddress());
            manager.setPhone_number(managerDto.getPhone_number());
            manager.setMatrimonial_status(managerDto.getMatrimonial_status());
            manager.setBirthday(managerDto.getBirthday());
            manager.setProfession(managerDto.getProfession());
            manager.setCode(GenerateCodeUtils.generateCode());
            manager.setCni(managerDto.getCni());
            manager.setPassword(managerDto.getPassword());
            String role = "manager";
            Bank_Role bank_role = bank_role_repository.findByRole(role);
            List<Bank_Role> bank_roles = new ArrayList<>();
            manager.setBank_roles(bank_roles);
            manager = managerRepository.save(manager);


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return manager;


    }

    public Page<Account> findAccountByManager(Pageable peageRequest) throws Exception {

        Manager manager = null;
        return accountRepository.findByManager(manager, peageRequest);


    }


    public void envoyerMailVerification(String mail, String contenu) throws MessagingException{
        MimeMessage message= emailSender.createMimeMessage();

        boolean multipart= true;

        MimeMessageHelper helper= new MimeMessageHelper(message,multipart,"utf-8");

        String htmlMsg = contenu;

        message.setContent(htmlMsg, "text/html");

        helper.setTo(mail);

        helper.setSubject("Activation du compte Bank");


        this.emailSender.send(message);

    }


    public Account ValidateAccount(String accountNumber) throws Exception {
        System.out.println(accountNumber);
       // Account account = new Account();
        Account account = accountRepository.findByAccountNumber(accountNumber);
        System.out.println(account.toString());


        if (account == null) {
            throw new Exception("ce numéro de compte n'existe pas");

        }
        //URL url = new URL().
        account.setStatus("actif");
        String subject = "Confirmation Mail";
        String text = "Hello Mr/Mrs" + account.getClient().getName() + "," + "your demand for the creation of your account has been approuved." +
                "here are your credential to log in your account: " + "account Number:" + account.getAccountNumber() + " and Username:  " +
                account.getClient().getEmail() +
                ". Welcome on MyBankOnline"
                ;


        envoyerMailVerification(account.getClient().getEmail(),text);


        return accountRepository.save(account);


    }

    // valider la demande de suppression d'un compte

    @Override
    public Demand_Suppression validateDemandSuppression(String accountNumber) throws Exception {
      Demand_Suppression demand_suppression= new Demand_Suppression();


        Account account = new Account();
        account = accountRepository.findByAccountNumber(accountNumber);
        if (account == null) {
            throw new Exception("ce numéro de compte n'existe pas");
        }

        demand_suppression= demandRepository.findByAccountNumber(accountNumber);

        String subject = "Confirm the suppression of your account";
        String text = "Hello Mr/Mrs" + account.getClient().getName() + "," + "your demand for the suppression of your account has been approuved.";

        try {

            account.setStatus("inactif");
            demand_suppression.setStatus("validate");

             accountRepository.save(account);

            envoyerMailVerification(account.getClient().getEmail(),text);
            //emailService.SendSimpleMessage(account.getClient().getEmail(),subject,text);



            return demandRepository.save(demand_suppression);

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }




    }
}
