package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.RetraitDto;
import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Retrait;
import com.isj.isi.Mybankbackend.repositories.AccountRepository;
import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@Service
public class RetraitService implements RetraitInterface {

    private Account account;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RetraitRepository retraitRepository;

    @Autowired
    private JavaMailSender emailSender;



    public void envoyerMailVerification(String mail, String contenu) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

        String htmlMsg = contenu;

        message.setContent(htmlMsg, "text/html");

        helper.setTo(mail);

        helper.setSubject("Confirm the withdrawal");


        this.emailSender.send(message);

    }





    @Override
    public Retrait addRetrait(RetraitDto retraitDto) throws Exception {

        account= accountRepository.findByAccountNumber(retraitDto.getAccountNumber());

            Retrait retrait= new Retrait();
            retrait.setAccountNumber(retraitDto.getAccountNumber());
            retrait.setName(retraitDto.getName());
            retrait.setSurname(retraitDto.getSurname());
           // retrait.setSecretCode(retraitDto.getSecretCode());
            retrait.setMontant(retraitDto.getMontant());
           float b=retrait.getMontant();
           float a= (account.getBalanceAccount());
            account.setBalanceAccount(a-b);

            accountRepository.save(account);
            retrait= retraitRepository.save(retrait);

        String text = "Hello Mr/Mrs" + account.getClient().getName() + "," + "The withdrawal of "+ retrait.getMontant()+ " has been done in your account. You stil have:"+
                account.getBalanceAccount() +
                "please verify your balance account ";

            envoyerMailVerification(account.getClient().getEmail(), text);

            return retrait;



    }

}
