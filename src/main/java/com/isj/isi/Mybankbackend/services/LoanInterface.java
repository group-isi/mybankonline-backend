package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.LoanDto;
import com.isj.isi.Mybankbackend.entity.Loan;

public interface LoanInterface {

    public Loan addLoan (LoanDto loanDto) throws Exception;
}
