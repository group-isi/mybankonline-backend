package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.DepotDto;
import com.isj.isi.Mybankbackend.entity.Depot;

public interface   DepotInterface {

    public Depot addDepot(DepotDto depotDto) throws Exception;
}
