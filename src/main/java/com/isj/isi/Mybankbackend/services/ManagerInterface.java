package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.ManagerDto;
import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Demand_Suppression;
import com.isj.isi.Mybankbackend.entity.Manager;
import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManagerInterface {

    public Manager addManager(ManagerDto managerDto) throws Exception;

    public Account ValidateAccount (String accountNumber) throws Exception;

    public Demand_Suppression validateDemandSuppression(String accountNumber) throws Exception;

    public Page<Account> findAccountByManager(Pageable peageRequest) throws Exception;

   // public void envoyerMailVerification(String mail, String contenu) throws MessagingException;
}
