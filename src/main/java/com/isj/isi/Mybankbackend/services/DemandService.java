package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.DemandDto;
import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Demand_Suppression;
import com.isj.isi.Mybankbackend.repositories.AccountRepository;
import com.isj.isi.Mybankbackend.repositories.DemandRepository;
import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class DemandService implements DemandInterface {

    @Autowired
    private DemandRepository demandRepository;

    @Autowired
    private AccountRepository accountRepository;



    public Demand_Suppression addDemand(DemandDto demandDto) throws Exception{
        Demand_Suppression demand_suppression= new Demand_Suppression();
          Account account= new Account();

          account=  accountRepository.findByAccountNumber(demandDto.getAccountNumber());

          if ( account==null){
              throw new Exception("ce numéro de compte n'existe pas");
          }

        System.out.println(account.toString());


        try{
            demand_suppression.setName(demandDto.getName());
            demand_suppression.setAccountNumber(demandDto.getAccountNumber());
            demand_suppression.setMotif_suppression(demandDto.getMotif_suppression());

            demand_suppression.setClient(account.getClient());
            System.out.println(demand_suppression.toString());
            demand_suppression.setManager(account.getManager());
            demand_suppression.setStatus("unvalidate");

            demand_suppression= demandRepository.save(demand_suppression);
            return demand_suppression;

        }catch (Exception ex) {
            ex.printStackTrace();

            return null;
        }

    }


    @Override
    public Page<Demand_Suppression> listDemandSuppression(Pageable peageRequest) throws Exception {

        String status= "unvalidate";
        return demandRepository.findByStatus(status, peageRequest);
    }
}
