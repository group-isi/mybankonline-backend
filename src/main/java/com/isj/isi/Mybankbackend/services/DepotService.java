package com.isj.isi.Mybankbackend.services;

import com.isj.isi.Mybankbackend.dto.DepotDto;
import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Depot;
import com.isj.isi.Mybankbackend.repositories.AccountRepository;
import com.isj.isi.Mybankbackend.repositories.ClientRepository;
import com.isj.isi.Mybankbackend.repositories.DepotRepository;
import com.isj.isi.Mybankbackend.repositories.RetraitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class DepotService {

    @Autowired
    private AccountRepository accountRepository;


    @Autowired
    private JavaMailSender emailSender;


    @Autowired
    private DepotRepository depotRepository;

@Autowired
private ClientRepository clientRepository;
    private Account account;


    public void envoyerMailVerification(String mail, String contenu) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

        String htmlMsg = contenu;

        message.setContent(htmlMsg, "text/html");

        helper.setTo(mail);

        helper.setSubject("Confirm the  Repository");


        this.emailSender.send(message);

    }


    public Depot addDepot(DepotDto depotDto) throws Exception {

        System.out.println(depotDto.getAccountNumber());
        account = accountRepository.findByAccountNumber(depotDto.getAccountNumber());


        System.out.println(account.toString());
        if (account == null) {
            throw new Exception("ce compte n'existe pas");
        }

        Depot depot = new Depot();


       String text = "Hello Mr/Mrs" + account.getClient().getName() + "," + "The repository hes been done in your account ." +
                "please verify your balance account ";


        try {

            depot.setAccountNumber(depotDto.getAccountNumber());
            depot.setMontant(depotDto.getMontant());
            depot.setEspece(depotDto.getEspece());
            float a = depot.getMontant();
          float b = account.getBalanceAccount();
          account.setBalanceAccount(a + b);
          accountRepository.save(account);
           depot= depotRepository.save(depot);


            envoyerMailVerification(account.getClient().getEmail(),text);

            return depot;

        } catch (Exception ex) {
            ex.printStackTrace();
            //System.out.println(ex.toString());
            return null;
        }


    }
}
