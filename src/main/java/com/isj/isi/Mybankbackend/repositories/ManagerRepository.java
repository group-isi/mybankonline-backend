package com.isj.isi.Mybankbackend.repositories;

import com.isj.isi.Mybankbackend.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {

    Manager findByEmail (String email);

   // Manager findByPhone_number(String phone_number);

}
