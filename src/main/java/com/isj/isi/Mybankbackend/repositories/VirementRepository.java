package com.isj.isi.Mybankbackend.repositories;

import com.isj.isi.Mybankbackend.entity.Virement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface VirementRepository extends JpaRepository<Virement, Long> {

}
