package com.isj.isi.Mybankbackend.repositories;

import com.isj.isi.Mybankbackend.entity.Account;
import com.isj.isi.Mybankbackend.entity.Manager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {


    Page<Account> findByManager (Manager manager, Pageable pageRequest);

    Account findByAccountNumber(String accountNumber);

}
