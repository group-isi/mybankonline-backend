package com.isj.isi.Mybankbackend.repositories;

import com.isj.isi.Mybankbackend.entity.Demand_Suppression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DemandRepository extends JpaRepository<Demand_Suppression,Long> {


Demand_Suppression findByAccountNumber (String accountNumber);

    Page<Demand_Suppression> findByStatus (String status, Pageable pageRequest);
}
