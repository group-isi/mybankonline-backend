package com.isj.isi.Mybankbackend.repositories;

import com.isj.isi.Mybankbackend.entity.Client;
import com.isj.isi.Mybankbackend.entity.Manager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.event.CaretListener;

public interface ClientRepository extends JpaRepository<Client,Long> {

    //Client findByPhone_number(String phone_number);

    Client findByEmail(String email);
Client findByName (String name);


}
