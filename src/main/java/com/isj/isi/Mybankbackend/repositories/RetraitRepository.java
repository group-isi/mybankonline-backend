package com.isj.isi.Mybankbackend.repositories;


import com.isj.isi.Mybankbackend.entity.Abstract_Mybank_Entity;
import com.isj.isi.Mybankbackend.entity.Client;
import com.isj.isi.Mybankbackend.entity.Manager;
import com.isj.isi.Mybankbackend.entity.Retrait;
import lombok.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@Repository
public interface RetraitRepository extends  JpaRepository<Retrait, Long> {

}
