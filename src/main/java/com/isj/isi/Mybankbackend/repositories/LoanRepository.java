package com.isj.isi.Mybankbackend.repositories;


import com.isj.isi.Mybankbackend.entity.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long> {


}
