package com.isj.isi.Mybankbackend.repositories;

import com.isj.isi.Mybankbackend.entity.Bank_Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface Bank_Role_Repository extends JpaRepository<Bank_Role, Long> {

    Bank_Role findByRole( String role);

}
