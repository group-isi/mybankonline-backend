package com.isj.isi.Mybankbackend.mails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;


@Service
public class EmailServiceImpl implements EmailService, Runnable{


    private JavaMailSender emailSender = new JavaMailSenderImpl();

    String to ="wilfriedbobi@gmail.com";
    String subject= "Greetings";
    String text= "bonjour";

    public void SendSimpleMessage(String to, String subject, String text){

        SimpleMailMessage message= new SimpleMailMessage();
       // message.setFrom("micheleateutchia@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);


    }

    public void envoyerMailVerification(String mail, String contenu) throws MessagingException{
        MimeMessage message= emailSender.createMimeMessage();

        boolean multipart= true;

        MimeMessageHelper helper= new MimeMessageHelper(message,multipart,"utf-8");

        String htmlMsg = contenu;

        message.setContent(htmlMsg, "text/html");

        helper.setTo(mail);

        helper.setSubject("Activation du compte Bank");


        this.emailSender.send(message);

    }


    public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment) throws MessagingException {


        MimeMessage message= emailSender.createMimeMessage();
        MimeMessageHelper helper= new MimeMessageHelper(message,true);

        helper.setFrom("micheleateutchia@gmail.com");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(text);
         FileSystemResource file= new FileSystemResource(new File(pathToAttachment));
         helper.addAttachment("Invoice", file);

         emailSender.send(message);
    }

    @Override
    public void run() {


    }
}
